# ElasticSearch 7.10.2 for Apple M1 with WMF plugins

This is the Dockerfile for an image that runs on Apple M1 with ElasticSearch 7.10.2 and the search/extra and search/highlighter plugins.

For local development only.

```sh
docker build . -t wmf-elasticsearch-arm64:7.10.2
```

And finally, you can run the image with:

```sh
docker run -d --name=wmf-elasticsearch7 --restart=always -v elasticdata:/usr/share/elasticsearch/data -e "discovery.type=single-node" -e "bootstrap.system_call_filter=false" -p 9200:9200 -p 9300:9300 wmf-elasticsearch-arm64:7.10.2
```

A prebuilt image on Docker Hub is also available at `kostajh/wmf-elasticsearch-arm64:7.10.2`
